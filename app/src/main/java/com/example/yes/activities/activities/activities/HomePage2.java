package com.example.yes.activities.activities.activities;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.EditText;
import android.widget.TextView;

import com.example.yes.R;
import com.example.yes.activities.activities.adapters.FragmentAdapater;
import com.example.yes.activities.activities.fragment.Tab2Fragment;
import com.example.yes.activities.activities.fragment.Tab3Fragment;
import com.example.yes.activities.activities.fragment.Tab4Fragment;
import com.example.yes.activities.activities.fragment.Tab5Fragment;
import com.example.yes.activities.activities.fragment.TabFragment;

public class HomePage2 extends AppCompatActivity {
private static  final String TAG="HomePage2";
private FragmentAdapater mfragmenadapter;
private  ViewPager mViewPager;
private TabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page2);

tabLayout =(TabLayout) findViewById(R.id.tabs);
mViewPager=(ViewPager) findViewById(R.id.container);
mfragmenadapter = new FragmentAdapater(getSupportFragmentManager());
mViewPager.setAdapter(mfragmenadapter);

        final TabLayout.Tab profile = tabLayout.newTab();
        final TabLayout.Tab quiz = tabLayout.newTab();
        final TabLayout.Tab lesson = tabLayout.newTab();
        final TabLayout.Tab teacher = tabLayout.newTab();
        final TabLayout.Tab video = tabLayout.newTab();

        profile.setIcon(R.drawable.video);
        quiz.setIcon(R.drawable.video);
        lesson.setIcon(R.drawable.video);
        teacher.setIcon(R.drawable.video);
        video.setIcon(R.drawable.video);

        tabLayout.addTab(profile,0);
        tabLayout.addTab(quiz,1);
        tabLayout.addTab(lesson,2);
        tabLayout.addTab(teacher,3);
        tabLayout.addTab(video,4);

       // tabLayout.setTabTextColors(ContextCompat.getColorStateList(this, R.color.tab_selector));
       // tabLayout.setSelectedTabIndicator(ContextCompat.getColor(this,R.color.colorAccent));
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        setupViewadapter(mViewPager);
    }

   private void setupViewadapter (ViewPager viewPager){
        String a = tabLayout.toString();
        FragmentAdapater adapater = new FragmentAdapater(getSupportFragmentManager());
        adapater.addFragment(new TabFragment(),a);
       adapater.addFragment(new Tab2Fragment(),a);
       adapater.addFragment(new Tab3Fragment(),a);
       adapater.addFragment(new Tab4Fragment(),a);
       adapater.addFragment(new Tab5Fragment(),a);
        viewPager.setAdapter(adapater);
   }
}
