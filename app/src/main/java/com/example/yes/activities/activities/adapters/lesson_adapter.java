package com.example.yes.activities.activities.adapters;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.yes.R;

import com.example.yes.activities.activities.Models.lessons;

import java.util.ArrayList;

public class lesson_adapter extends RecyclerView.Adapter<lesson_adapter.ViewHolder> {
    private Context mContext;
    private ArrayList<lessons> mList;
    public lesson_adapter(Context context, ArrayList<lessons> list){
        mContext = context;
        mList = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);

        View view = layoutInflater.inflate(R.layout.lesson_list,viewGroup, false);

   ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        final lessons quizzes = mList.get(i);


        TextView tvLessons1, tclessons, tvlessons2;
        Button btnview;

        tvLessons1 = viewHolder.tvLessons;
        tclessons = viewHolder.tvLesson1;
        tvlessons2 = viewHolder.tvLesson2;




        tvLessons1.setText(quizzes.getQuestion());
        tclessons.setText( quizzes.getChoice1());
        tvlessons2.setText(quizzes.getChoice2());


    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvLesson1, tvLesson2,tvLessons;

        public ViewHolder(@NonNull View itemView) {

            super(itemView);
            tvLessons = (TextView) itemView.findViewById(R.id.tvLessons);
            tvLesson1 = (TextView) itemView.findViewById(R.id.tvlesson1);
            tvLesson2 = (TextView) itemView.findViewById(R.id.tvlesson2);

        }
    }
}
