package com.example.yes.activities.activities.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.yes.R;
import com.example.yes.activities.activities.Models.Quizzes;

import java.util.ArrayList;

public class QuizAdapter extends RecyclerView.Adapter<QuizAdapter.ViewHolder>  {

    private Context mContext;
    private ArrayList<Quizzes> mList;
    public QuizAdapter(Context context, ArrayList<Quizzes> list){
        mContext = context;
        mList = list;
    }
    @NonNull
    @Override
    public QuizAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);

        View view = layoutInflater.inflate(R.layout.listrow_quiz,viewGroup, false);

        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull QuizAdapter.ViewHolder viewHolder, int i) {
        final Quizzes quizzes = mList.get(i);


        TextView tvQuestion, tvChoice1, tvChoice2, tvChoice3,tvChoice4;
        Button btnview;

        tvQuestion = viewHolder.tvQuestion;
        tvChoice1 = viewHolder.tvChoice1;
        tvChoice2 = viewHolder.tvChoice2;
        tvChoice3 = viewHolder.tvChoice3;
        tvChoice4 = viewHolder.tvChoice4;



        tvQuestion.setText(quizzes.getQuestion());
        tvChoice1.setText("A. " + quizzes.getChoice1());
        tvChoice2.setText("B. " +quizzes.getChoice2());
        tvChoice3.setText("C. " +quizzes.getChoice3());
        tvChoice4.setText("D. " +quizzes.getChoice4());
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }
    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvQuestion, tvChoice1, tvChoice2, tvChoice3, tvChoice4;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvQuestion = (TextView) itemView.findViewById(R.id.tvQuestion);
            tvChoice1 = (TextView) itemView.findViewById(R.id.tvChoice1);
            tvChoice2 = (TextView) itemView.findViewById(R.id.tvChoice2);
            tvChoice3 = (TextView) itemView.findViewById(R.id.tvChoice3);
            tvChoice4 = (TextView) itemView.findViewById(R.id.tvChoice4);
        }
    }
}
