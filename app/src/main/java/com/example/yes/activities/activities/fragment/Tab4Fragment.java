package com.example.yes.activities.activities.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.yes.R;
import com.example.yes.activities.activities.Models.Quizzes;
import com.example.yes.activities.activities.Models.lessons;
import com.example.yes.activities.activities.adapters.QuizAdapter;
import com.example.yes.activities.activities.adapters.lesson_adapter;

import java.util.ArrayList;

public class Tab4Fragment extends Fragment {
    private static final String TAG = "Tab3Fragment";
    private Button btnTab4;
    private View view;
    private Context context;
    private RecyclerView recyclerView;
    private ArrayList<lessons> LessonsList;
    //private QuizAdapter quizAdapter;
    private lesson_adapter lessonAdapter;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
       view = inflater.inflate(R.layout.tab_4,container,false);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        setHasOptionsMenu(true);
        context = getContext();
        initializeUI();
    }

    private void initializeUI() {
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView1);

        LessonsList = new ArrayList<>();
        LessonsList.add(new lessons("Lesson1", "math", "english"));
        LessonsList.add(new lessons("Lesson2", "psycho", "MUSIC"));
        LessonsList.add(new lessons("Lesson3", "geometry", "MAPEH"));
        LessonsList.add(new lessons("Lesson4", "science", "TLE"));
        LessonsList.add(new lessons("Lesson5", "filipino", "AP"));

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        RecyclerView.LayoutManager rvLayoutManager = layoutManager;
        recyclerView.setLayoutManager(rvLayoutManager);

        lessonAdapter = new lesson_adapter(getContext(), LessonsList);

        recyclerView.setAdapter(lessonAdapter);
    }
}
