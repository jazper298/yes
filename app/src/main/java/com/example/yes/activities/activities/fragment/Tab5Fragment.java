package com.example.yes.activities.activities.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.yes.R;

public class Tab5Fragment extends Fragment {
    private static  int VIDEO_REQUEST = 101;
    private static final String TAG = "Tab5Fragment";
    private Button btnTab5;
    Context context;
    private ImageView imageView;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tab_5,container,false);
        PackageManager pm = getActivity().getPackageManager();


        imageView = view.findViewById(R.id.imVideo);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent videoIntnt = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                if(videoIntnt.resolveActivity(getActivity().getPackageManager()) != null){
                    startActivityForResult(videoIntnt,VIDEO_REQUEST);
                }
            }
        });

        return view;
    }

}
