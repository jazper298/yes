package com.example.yes.activities.activities.activities;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.example.yes.R;

import java.util.Random;

public class MainActivity extends AppCompatActivity {
    private ConstraintLayout linearLayout;
Button login,create;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        linearLayout = (ConstraintLayout) findViewById(R.id.background);

        linearLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                String [] color ={
                        "#09216B",
                        "#3c1053",
                        "#4b134f",
                        "#642B73",
                        "#45a247",
                        "#8e44ad"

                };
                String [] color2 ={
                        "#DA4453",
                        "#ad5389",
                        "#C6426E",
                        "#159957",
                        "#1CB5E0",
                        "#1CB5E0"

                };

                Random random = new Random();
                GradientDrawable gradientDrawable = new GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, new int[]{Color.parseColor(color[random.nextInt(6)]),Color.parseColor(color2[random.nextInt(6)])}
                );
                linearLayout.setBackground(gradientDrawable);
                return false;
            }
        });
        login = (Button)findViewById(R.id.btnLog);
        create = (Button) findViewById(R.id.Create);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            Intent inten = new Intent(MainActivity.this, HomePage2.class);
            startActivity(inten);
            }
        });
        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent create = new Intent(MainActivity.this,studRegistration.class);
                startActivity(create);
            }
        });
        Spinner levell = (Spinner) findViewById(R.id.spinner);

    }
}
