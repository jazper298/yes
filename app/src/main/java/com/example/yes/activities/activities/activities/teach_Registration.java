package com.example.yes.activities.activities.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;

import com.example.yes.R;
import com.example.yes.activities.activities.activities.utils.CodeDialog;

public class teach_Registration extends AppCompatActivity {
    private Context context;
    Button submit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teach__registration);
        context=this;
        submit =(Button) findViewById(R.id.btnSubmit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                opendialog();

            }
        });

    }
    public void opendialog()
    {
        CodeDialog codeDialog = new CodeDialog();
        codeDialog.show(getSupportFragmentManager(),"Code Verify");
    }
}
