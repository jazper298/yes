package com.example.yes.activities.activities.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.yes.R;
import com.example.yes.activities.activities.Models.Quizzes;
import com.example.yes.activities.activities.adapters.QuizAdapter;

import java.util.ArrayList;

public class Tab2Fragment extends Fragment {
    private static final String TAG = "Tab2Fragment";
    private Button btnTab2;

    private View view;
    private Context context;
    private RecyclerView recyclerView;
    private ArrayList<Quizzes> quizzesList;
    private QuizAdapter quizAdapter;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
         view = inflater.inflate(R.layout.fragment_tab2,container,false);
       // setPageTitle("Quiz");

        return view;
    }
//    public void setPageTitle(String title) {
//        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowCustomEnabled(false);
//        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(Html.fromHtml("<font color='#ffffff'>"+title+ "</font>"));
//    }
    @Override
    public void onStart() {
        super.onStart();
        setHasOptionsMenu(true);
        context = getContext();
        initializeUI();
    }
    private void initializeUI() {
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);

        quizzesList = new ArrayList<>();
        quizzesList.add(new Quizzes("1. Batman is a gay", "true", "false", "maybe", "i dont know"));
        quizzesList.add(new Quizzes("2. Batman is a gay", "true", "false", "maybe", "i dont know"));
        quizzesList.add(new Quizzes("3. Batman is a gay", "true", "false", "maybe", "i dont know"));
        quizzesList.add(new Quizzes("4. Batman is a gay", "true", "false", "maybe", "i dont know"));
        quizzesList.add(new Quizzes("5. Batman is a gay", "true", "false", "maybe", "i dont know"));

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        RecyclerView.LayoutManager rvLayoutManager = layoutManager;
        recyclerView.setLayoutManager(rvLayoutManager);

        quizAdapter = new QuizAdapter(getContext(), quizzesList);

        recyclerView.setAdapter(quizAdapter);
    }
}
