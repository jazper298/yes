package com.example.yes.activities.activities.activities.utils;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.example.yes.R;
import com.example.yes.activities.activities.activities.HomePage2;
import com.example.yes.activities.activities.activities.Homepage;

public class CodeDialog extends AppCompatDialogFragment {

    private EditText Codes;
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.verify_code,null);

        builder.setView(view)
                .setTitle("Code")
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setPositiveButton("Send", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
Intent inten = new Intent (getContext(), HomePage2.class);
startActivity(inten);
                    }
                });
Codes = view.findViewById(R.id.etCode);
                return builder.create();

    }
}
