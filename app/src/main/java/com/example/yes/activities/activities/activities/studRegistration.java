package com.example.yes.activities.activities.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.yes.R;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class studRegistration extends AppCompatActivity {
Button save,cancel;
    EditText etFullName, contactAddress, etEmail, etUsername, etPassword;
    String strUrl;
    OkHttpClient okHttpClient;
    Request request;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stud_registration);

        save = (Button) findViewById(R.id.btnStudSave);
        cancel = (Button) findViewById(R.id.btnCancel);
        etFullName =(EditText) findViewById(R.id.etStudFname) ;
        contactAddress =(EditText) findViewById(R.id.etTEachContact) ;
        etEmail =(EditText) findViewById(R.id.etTeachEmail) ;
        etUsername = (EditText) findViewById(R.id.etStudUsername);
        etPassword =(EditText) findViewById(R.id.etStudPass);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                strUrl = getString(R.string.url) + "studRegistration.php";
                //strUrl = getString(R.string.url) + "saveuser.php";
                HttpUrl.Builder urlBuilder = HttpUrl.parse(strUrl).newBuilder();
                urlBuilder.addQueryParameter("studFullname", etFullName.getText().toString());
                urlBuilder.addQueryParameter("studContact", contactAddress.getText().toString());
                urlBuilder.addQueryParameter("studEmail", etEmail.getText().toString());
                urlBuilder.addQueryParameter("studUser", etUsername.getText().toString());
                urlBuilder.addQueryParameter("studPass", save.getText().toString());
                strUrl = urlBuilder.build().toString();

                okHttpClient = new OkHttpClient();
                request = new Request.Builder().url(strUrl).build();
                okHttpClient.newCall(request).enqueue(new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {

                    }

                    @Override
                    public void onResponse(Call call, Response response) throws IOException {
                        if (response.isSuccessful()){
                            final String responseMessage = response.body().string();
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (responseMessage.equals("1")){
                                        Toast.makeText(studRegistration.this, "YOU ARE SUCCESSFULLY REGISTERED.", Toast.LENGTH_SHORT).show();
                                        etFullName.setText("");
                                        contactAddress.setText("");
                                        etEmail.setText("");
                                        etUsername.setText("");
                                        etPassword.setText("");
                                        Intent save = new Intent(studRegistration.this,MainActivity.class);
                                        startActivity(save);
                                        finish();
                                    }
                                    else {
                                        Toast.makeText(studRegistration.this, "YOU ARE NOT SUCCESSFULLY REGISTERED.", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                        }

                    }
                });

            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cancel = new Intent(studRegistration.this,MainActivity.class);
                startActivity(cancel);

            }
        });
    }
}
