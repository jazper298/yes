package com.example.yes.activities.activities.Models;

public class lessons {
    private String lesson, lesson1, lesson2;

    public lessons(String question, String choice1, String choice2) {
        this.lesson = question;
        this.lesson1 = choice1;
        this.lesson2 = choice2;


    }

    public String getQuestion() {
        return lesson;
    }

    public String getChoice1() {
        return lesson1;
    }

    public String getChoice2() {
        return lesson2;
    }


    public void setQuestion(String question) {
        this.lesson = question;
    }

    public void setChoice1(String choice1) {
        this.lesson1 = choice1;
    }

    public void setChoice2(String choice2) {
        this.lesson2 = choice2;
    }
}


